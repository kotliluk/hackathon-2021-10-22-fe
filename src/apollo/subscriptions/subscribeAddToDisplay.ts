import { FetchResult, gql } from '@apollo/client'
import { client } from '../client'
import { DisplayRow } from '../../models/displayRow'
import { Observable } from '@apollo/client/utilities/observables/Observable'
import { config } from '../../config'


interface AddToDisplayData {
  addToDisplay: DisplayRow
}

const ADD_TO_DISPLAY_SUBSCRIPTION = gql`
  subscription Subscription($displayLocation: DisplayLocation) {
    addToDisplay(displayLocation: $displayLocation) {
      id
      rpToDisplay
      targetToDisplay
      createdAt
      expireAt
      targetType
      displayLocation
    }
  }
`

/**
 * Subscribes to added row. Returns observer, which publishes data as { data: { addToDisplay: DisplayRow } }.
 */
export const subscribeAddToDisplay = (): Observable<FetchResult<AddToDisplayData>> => {
  return client.subscribe<AddToDisplayData, {}>(
    { query: ADD_TO_DISPLAY_SUBSCRIPTION, variables: { displayLocation: config.displayLocation } }
  )
}
