import { FetchResult, gql } from '@apollo/client'
import { client } from '../client'
import { Observable } from '@apollo/client/utilities/observables/Observable'
import { config } from '../../config'


interface RemoveFromDisplayData {
  removeFromDisplay: {
    id: number,
  }
}

const REMOVE_FROM_DISPLAY_SUBSCRIPTION = gql`
  subscription Subscription($displayLocation: DisplayLocation) {
    removeFromDisplay(displayLocation: $displayLocation) {
      id
    }
  }
`

/**
 * Subscribes to removed rows. Returns observer which publishes data as { data: { removeFromDisplay: { id: number } } }.
 */
export const subscribeRemoveFromDisplay = (): Observable<FetchResult<RemoveFromDisplayData>> => {
  return client.subscribe<RemoveFromDisplayData, {}>(
    { query: REMOVE_FROM_DISPLAY_SUBSCRIPTION, variables: { displayLocation: config.displayLocation } }
  )
}
