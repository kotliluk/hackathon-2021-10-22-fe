import { ApolloQueryResult, gql } from '@apollo/client'
import { client } from '../client'
import { Product } from '../../models/product'


export interface ProductsData {
  getProductsForCategory: Product[]
}

export interface ProductsVars {
  categoryId: number
}

export const PRODUCTS_FOR_CATEGORY_QUERY = gql`
  query ProductsData($categoryId: Int!) {
    getProductsForCategory(categoryId: $categoryId) {
      id
      name
      description
      categoryId
      imageUrl
      price
      amount
    }
  }
`

export const fetchCategories = async (categoryId: number): Promise<ApolloQueryResult<ProductsData>> => {
  return await client.query<ProductsData, ProductsVars>(
    { query: PRODUCTS_FOR_CATEGORY_QUERY, variables: { categoryId } }
  )
}
