import { ApolloQueryResult, gql } from '@apollo/client'
import { client } from '../client'
import { Product } from '../../models/product'


export interface AllProductsData {
  getAllProducts: Product[]
}

export const ALL_PRODUCTS_QUERY = gql`
  query Query {
    getAllProducts {
      name
      id
      description
      categoryId
      imageUrl
      price
      amount
    }
  }
`

export const fetchCategories = async (): Promise<ApolloQueryResult<AllProductsData>> => {
  return await client.query<AllProductsData, {}>({ query: ALL_PRODUCTS_QUERY })
}
