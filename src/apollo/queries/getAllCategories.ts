import { ApolloQueryResult, gql } from '@apollo/client'
import { client } from '../client'
import { Category } from '../../models/category'


export interface CategoriesData {
  getAllCategories: Category[]
}

export const CATEGORIES_QUERY = gql`
  query CategoriesData {
    getAllCategories {
      id
      name
    }
  }
`

export const fetchCategories = async (): Promise<ApolloQueryResult<CategoriesData>> => {
  return await client.query<CategoriesData, {}>({ query: CATEGORIES_QUERY })
}
