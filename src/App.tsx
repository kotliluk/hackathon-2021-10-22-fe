// eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental
import React from 'react'
import MainScreen from './components/mainScreen/MainScreen'
import './App.scss'


function App (): JSX.Element {
  return (
    <div className="App">
      <MainScreen/>
    </div>
  )
}

export default App
