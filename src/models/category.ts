/**
 * Data needed for one category
 */
export type Category = {
  id: number,
  name: string,
}
