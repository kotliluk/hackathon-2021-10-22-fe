/**
 * Data needed for one product
 */
export type Product = {
  id: number,
  name: string,
  description: string,
  categoryId: number,
  pictureUrl: string,
  price: number,
  amount: number,
}
