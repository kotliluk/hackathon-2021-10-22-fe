/**
 * Type of the displayed information.
 */
export enum DisplayTargetType {
  PARKING = 'PARKING',
  RAMP = 'RAMP',
}

/**
 * Location of the display with the information.
 */
export enum DisplayLocation {
  GATEHOUSE = 'GATEHOUSE',
  PARKING = 'PARKING',
}

/**
 * Data to be displayed in a row.
 */
export type DisplayRow = {
  id: number,
  rpToDisplay: string,
  targetToDisplay: string,
  createdAt: Date,
  expireAt: Date,
  targetType: DisplayTargetType,
  displayLocation: DisplayLocation,
}
