import { DisplayLocation } from './models/displayRow'


/**
 * Centralized overall configuration of the application.
 */
export interface Config {
  // HTTP URI for GraphQL queries
  uriHttp: string
  // WebSocket URI for GraphQL subscriptions
  uriWs: string
  // location of the display
  displayLocation: DisplayLocation
  // settings of scrolling of too many rows
  autoscroll: {
    // interval between pixel shift in auto-scroll (in milliseconds)
    interval: number,
    // pause on top and on bottom of the column in auto-scroll (in milliseconds)
    pause: number,
  }
}

export const config: Config = {
  uriHttp: 'http://localhost:8081/graphql',
  uriWs: 'ws://localhost:8081/graphql',
  displayLocation: DisplayLocation.GATEHOUSE,
  autoscroll: {
    interval: 50,
    pause: 2000,
  },
}
