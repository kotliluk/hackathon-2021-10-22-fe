// eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental
import React from 'react'
import { Product } from '../../models/product'
// @ts-expect-error - e
import img1 from '../../assets/img/apple.jpg'
// @ts-expect-error - e
import img2 from '../../assets/img/tomato.jpg'
// @ts-expect-error - e
import img0 from '../../assets/img/beer.png'
import './ProductItem.scss'


interface ProductProps {
  product: Product
  onSelected: (id: number) => void
}

/**
 * Main screen of the app.
 */
function ProductItem (props: ProductProps): JSX.Element {

  let img
  switch (props.product.categoryId) {
    case 0:
      img = img0
      break
    case 1:
      img = img1
      break
    case 2:
      img = img2
      break
  }

  return (
    <div className="productItem" onClick={() => props.onSelected(props.product.id)}>
      <img className="icon" src={img}/>
      <span>{props.product.name}</span>
    </div>
  )
}

export default ProductItem
