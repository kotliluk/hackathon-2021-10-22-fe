// eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental
import React from 'react'
import './MainPanel.scss'
import ProductItem from '../productItem/ProductItem'
import { Product } from '../../models/product'


interface ProductProps {
  products: Product[]
  onSelectedProduct: (id: number) => void
}

/**
 * Main panel of the app.
 */
function MainPanel (props: ProductProps): JSX.Element {

  return (
    <div>
      <main className="main-panel">
        {props.products.map(productItem => (
          <ProductItem key={productItem.id} product={productItem} onSelected={props.onSelectedProduct} />
        ))}
      </main>
    </div>
  )
}

export default MainPanel
