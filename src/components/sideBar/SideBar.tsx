// eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental
import React from 'react'
import './SideBar.scss'
import CategoryItem from '../categoryItem/CategoryItem'
import { Category } from '../../models/category'


interface SideBarProps {
  onSelectedCategory: (id: number) => void
  categories: Category[]
}

/**
 * Main panel of the app.
 */
function SideBar (props: SideBarProps): JSX.Element {
  return (
    <div>
      <main className="side-bar">
        {props.categories.map(categoryItem => (
          <CategoryItem key={categoryItem.id} category={categoryItem} onSelected={props.onSelectedCategory}/>
        ))}
      </main>
    </div>
  )
}

export default SideBar
