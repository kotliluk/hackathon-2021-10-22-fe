// eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental
import React, { useEffect, useState } from 'react'
import './MainScreen.scss'
import MainPanel from '../mainPanel/MainPanel'
import SideBar from '../sideBar/SideBar'
import { useQuery } from '@apollo/client'
import { CategoriesData, CATEGORIES_QUERY } from '../../apollo/queries/getAllCategories'
import DetailPanel from '../detailPanel/DetailPanel'
import { ALL_PRODUCTS_QUERY, AllProductsData } from '../../apollo/queries/getAllProducts'

/**
 * Main screen of the app.
 */
function MainScreen (): JSX.Element {

  const [selectedCategoryId, setSelectedCategory] = useState(-1)
  const [selectedProductId, setSelectedProduct] = useState(-1)

  const { loading: categoriesLoading, data: categoriesData } = useQuery<CategoriesData>(CATEGORIES_QUERY)
  const { data: productsData } = useQuery<AllProductsData>(ALL_PRODUCTS_QUERY)
  const categories = (categoriesData === undefined || categoriesData.getAllCategories === undefined)
    ? []
    : categoriesData.getAllCategories
  const products = (productsData === undefined || productsData.getAllProducts === undefined)
    ? []
    : productsData.getAllProducts

  useEffect(() => {
    if (categoriesData?.getAllCategories !== undefined && categoriesData.getAllCategories.length > 0) {
      setSelectedCategory(0)
    }
  }, [categoriesLoading])

  const currentProducts = products.filter(p => p.categoryId === selectedCategoryId)
  const selectedProduct = products.find(p => p.id === selectedProductId)

  return (
    <main className="main-screen">
      {
        selectedProduct === undefined
          ? (
            <>
              <SideBar categories={categories} onSelectedCategory={setSelectedCategory} />
              <MainPanel products={currentProducts} onSelectedProduct={setSelectedProduct}/>
            </>
          )
          : <DetailPanel product={selectedProduct} onSelected={setSelectedProduct}/>
      }
    </main>
  )
}

export default MainScreen
