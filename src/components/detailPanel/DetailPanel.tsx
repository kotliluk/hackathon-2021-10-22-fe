// eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental
import React from 'react'
import { Product } from '../../models/product'
// @ts-expect-error - image
import img1 from '../../assets/img/apple.jpg'
// @ts-expect-error - image
import img2 from '../../assets/img/tomato.jpg'
// @ts-expect-error - image
import img0 from '../../assets/img/beer.png'
import './DetailPanel.scss'


interface DetailProps {
  product: Product
  onSelected: (id: number) => void
}

/**
 * Main panel of the app.
 */
function DetailPanel (props: DetailProps): JSX.Element {


  // const rows = useSelector(selectRows)
  let img
  switch (props.product.categoryId) {
    case 0:
      img = img0
      break
    case 1:
      img = img1
      break
    case 2:
      img = img2
      break
  }

  return (
    <main className="detail-panel">
      <div className="row">
        <div className="info">
          <div className="row textRow">
            <span>Name</span>
            <span>{props.product.name}</span>
          </div>
          <div className="row textRow">
            <span>Price</span>
            <span>{props.product.price}</span>
          </div>
          <div className="row textRow">
            <span>Amount</span>
            <span>{props.product.amount}</span>
          </div>
        </div>
        <img src={img}/>
      </div>
      <div>
        <div className="info"> {props.product.description}</div>
        <button className="my-button" onClick={() => props.onSelected(-1)}>
          <span>Back</span>
        </button>
      </div>
    </main>
  )
}

export default DetailPanel
