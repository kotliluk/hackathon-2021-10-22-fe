// eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental
import React from 'react'
import { Category } from '../../models/category'
import './CategoryItem.scss'


interface CategoryProps {
  category: Category
  onSelected: (id: number) => void
}

function CategoryItem (props: CategoryProps): JSX.Element {
  return (
    <button className="categoryItem" onClick={() => props.onSelected(props.category.id)}>
      <span>{props.category.name}</span>
    </button>
  )
}

export default CategoryItem
