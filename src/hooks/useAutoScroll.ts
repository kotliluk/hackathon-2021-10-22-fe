import React, { useCallback, useEffect, useRef, useState } from 'react'

/**
 * Auto-scrolls the given element if its content cannot fit inside.
 *
 * @param interval interval between pixel shift in auto-scroll (in milliseconds)
 * @param pause pause on top and on bottom of the column in auto-scroll (in milliseconds)
 * @return returns an array with created React reference object and an updating function which forces scrolling update
 * when called - use it to add additional dependencies for scrolling (dependencies implemented in a hook are a change of
 * hooks parameters and window resize)
 *
 * @example
 * const [elementRef, updateAutoScroll] = useAutoScroll(interval, pause);
 * useEffect(updateAutoScroll, [updateAutoScroll, my, additional, dependencies]);
 */
const useAutoScroll = <T extends HTMLElement>(interval: number, pause: number): [React.RefObject<T>, () => void] => {

  const elementRef = useRef<T>(null)
  const [scrollTop, setScrollTop] = useState(0)
  const [isScrolling, setIsScrolling] = useState(false)

  /**
   * Sets isScrolling to true iff the element needs to be scrolled.
   */
  const updateAutoScroll = useCallback(() => {
    const element = elementRef.current
    if (element) {
      setIsScrolling(element.scrollHeight > element.clientHeight)
    }
  }, [elementRef])

  /**
   * Adds window-resize-listener to trigger updateAutoScroll.
   */
  useEffect(() => {
    updateAutoScroll()
    window.addEventListener('resize', updateAutoScroll)
    return () => window.removeEventListener('resize', updateAutoScroll)
  }, [updateAutoScroll])

  /**
   * Auto-scrolls the TransportCards if they do not fit into the column at once.
   */
  useEffect(() => {
    const element = elementRef.current
    if (!element) {
      return
    }
    // if the scroll is needed
    if (isScrolling) {
      const maxScroll = element.scrollHeight - element.clientHeight

      let iid: NodeJS.Timeout
      // if the column is on top, uses longer pause
      if (scrollTop === 0) {
        iid = setTimeout(() => {
          element.scrollTo({ top: 1, behavior: 'smooth' })
          setScrollTop(1)
        }, pause)
      } else if (scrollTop < maxScroll) {
        // when not on top nor on bottom, uses basic scroll interval
        iid = setTimeout(() => {
          const newScrollTop = (scrollTop + 1 > maxScroll) ? maxScroll : (scrollTop + 1)
          element.scrollTo({ top: newScrollTop, behavior: 'smooth' })
          setScrollTop(newScrollTop)
        }, interval)
      } else {
        // if the column is on bottom, uses longer pause
        iid = setTimeout(() => {
          element.scrollTo({ top: 0, behavior: 'smooth' })
          setScrollTop(0)
        }, pause)
      }

      return () => clearInterval(iid)
    } else {
      // if the scroll is not needed
      setScrollTop(0)
    }
  }, [elementRef, interval, pause, scrollTop, isScrolling])

  // returns an element reference and an updating function
  return [elementRef, updateAutoScroll]
}

export default useAutoScroll
