import { RootState } from '../state'
import { DisplayRow } from '../../models/displayRow'

/**
 * Selects all row data.
 */
export const selectRows = (state: RootState): DisplayRow[] => {
  return state.rows.rows
}
