import { DisplayRow } from '../../models/displayRow'


export const initialState: State = {
  rows: [],
}

export interface State {
  rows: DisplayRow[]
}
