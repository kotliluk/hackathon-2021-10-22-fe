import { Action } from 'redux'
import { ThunkAction } from '../thunk'
import { subscribeAddToDisplay } from '../../apollo/subscriptions/subscribeAddToDisplay'
import { subscribeRemoveFromDisplay } from '../../apollo/subscriptions/subscribeRemoveFromDisplay'
import { DisplayRow } from '../../models/displayRow'


export type Actions = AddDisplayRowAction | DeleteDisplayRowAction | SubscribeToDisplayRowAction

/** ******************* Add row data *********************/

export const ADD_ROW_DATA = 'rows/ADD_ROW_DATA'

interface AddDisplayRowAction extends Action<typeof ADD_ROW_DATA> {
  payload: {
    row: DisplayRow,
  }
}

export const addDisplayRow = (row: DisplayRow): AddDisplayRowAction => {
  return {
    type: ADD_ROW_DATA,
    payload: {
      row,
    },
  }
}

/** ******************* Delete row data *********************/

export const DELETE_ROW_DATA = 'rows/DELETE_ROW_DATA'

interface DeleteDisplayRowAction extends Action<typeof DELETE_ROW_DATA> {
  payload: {
    displayRowId: number,
  }
}

export const deleteDisplayRow = (displayRowId: number): DeleteDisplayRowAction => {
  return {
    type: DELETE_ROW_DATA,
    payload: {
      displayRowId,
    },
  }
}

/** ******************* Subscribes to a row data changes *********************/

export const SUBSCRIBE_TO_ROW_DATA_CHANGES = 'rows/SUBSCRIBE_TO_ROW_DATA_CHANGES'

interface SubscribeToDisplayRowAction extends Action<typeof SUBSCRIBE_TO_ROW_DATA_CHANGES> {
  payload: {}
}

/**
 * Thunk action which subscribes to row data changes.
 */
export const subscribeToDisplayRow = (): ThunkAction => (dispatch) => {
  try {
    subscribeAddToDisplay().subscribe(result => {
      if (!result || !result.data) {
        throw Error('No data')
      }
      dispatch(addDisplayRow(result.data.addToDisplay))
    })

    subscribeRemoveFromDisplay().subscribe(result => {
      if (!result || !result.data) {
        throw Error('No data')
      }
      dispatch(deleteDisplayRow(result.data.removeFromDisplay.id))
    })
  } catch (e) {
    console.error(e)
  }
}
