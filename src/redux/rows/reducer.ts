import { initialState, State } from './state'
import { Actions, ADD_ROW_DATA, DELETE_ROW_DATA } from './actions'


// eslint-disable-next-line @typescript-eslint/default-param-last
export function reducer (state = initialState, action: Actions): State {
  switch (action.type) {
    case ADD_ROW_DATA:
      return {
        rows: [
          ...state.rows,
          {
            ...action.payload.row,
          },
        ],
      }

    case DELETE_ROW_DATA:
      return {
        rows: state.rows.filter(row => row.id !== action.payload.displayRowId),
      }

    default:
      return state
  }
}
