import { reducer as locationsReducer } from './rows/reducer'
import { Dispatch, Store, configureStore } from '@reduxjs/toolkit'
import { RootState } from './state'
import { Actions } from './actions'
// import { addDisplayRow, deleteDisplayRow } from './rows/actions'
// import { DisplayLocation, DisplayTargetType } from '../models/displayRow'


export const store: Store<RootState, Actions> = configureStore({
  reducer: {
    rows: locationsReducer,
  },
})

export type AppDispatch = Dispatch<Actions>

/** TESTING STATIC DATA **/

/* const actualDate = new Date()

store.dispatch(addDisplayRow({
  id: 1,
  rpToDisplay: 'AAA 1111',
  targetToDisplay: 'P',
  createdAt: actualDate,
  expireAt: actualDate,
  targetType: DisplayTargetType.PARKING,
  displayLocation: DisplayLocation.GATEHOUSE,
}))

store.dispatch(addDisplayRow({
  id: 2,
  rpToDisplay: 'AAA 2222',
  targetToDisplay: 'A2',
  createdAt: actualDate,
  expireAt: actualDate,
  targetType: DisplayTargetType.RAMP,
  displayLocation: DisplayLocation.PARKING,
}))

store.dispatch(addDisplayRow({
  id: 3,
  rpToDisplay: 'AAA 3333',
  targetToDisplay: 'P',
  createdAt: actualDate,
  expireAt: actualDate,
  targetType: DisplayTargetType.PARKING,
  displayLocation: DisplayLocation.GATEHOUSE,
}))

store.dispatch(addDisplayRow({
  id: 4,
  rpToDisplay: 'AAA 4444',
  targetToDisplay: 'A4',
  createdAt: actualDate,
  expireAt: actualDate,
  targetType: DisplayTargetType.RAMP,
  displayLocation: DisplayLocation.PARKING,
}))

setTimeout(() => {
  store.dispatch(deleteDisplayRow(3))
}, 2000)

setTimeout(() => {
  store.dispatch(addDisplayRow({
    id: 5,
    rpToDisplay: 'AAA 5555',
    targetToDisplay: 'A5',
    createdAt: actualDate,
    expireAt: actualDate,
    targetType: DisplayTargetType.RAMP,
    displayLocation: DisplayLocation.PARKING,
  }))
}, 3000)

setTimeout(() => {
  store.dispatch(addDisplayRow({
    id: 6,
    rpToDisplay: 'AAA 6666',
    targetToDisplay: 'A6',
    createdAt: actualDate,
    expireAt: actualDate,
    targetType: DisplayTargetType.RAMP,
    displayLocation: DisplayLocation.PARKING,
  }))
}, 4000)

setTimeout(() => {
  store.dispatch(addDisplayRow({
    id: 7,
    rpToDisplay: 'AAA 7777',
    targetToDisplay: 'A7',
    createdAt: actualDate,
    expireAt: actualDate,
    targetType: DisplayTargetType.RAMP,
    displayLocation: DisplayLocation.PARKING,
  }))
}, 5000)

setTimeout(() => {
  store.dispatch(addDisplayRow({
    id: 8,
    rpToDisplay: 'AAA 8888',
    targetToDisplay: 'A8',
    createdAt: actualDate,
    expireAt: actualDate,
    targetType: DisplayTargetType.RAMP,
    displayLocation: DisplayLocation.PARKING,
  }))
}, 6000)

setTimeout(() => {
  store.dispatch(addDisplayRow({
    id: 9,
    rpToDisplay: 'AAA 9999',
    targetToDisplay: 'A9',
    createdAt: actualDate,
    expireAt: actualDate,
    targetType: DisplayTargetType.RAMP,
    displayLocation: DisplayLocation.PARKING,
  }))
}, 7000)

setTimeout(() => {
  store.dispatch(deleteDisplayRow(7))
}, 8000)*/
