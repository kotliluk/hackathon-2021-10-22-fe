import { Actions as RowsActions } from './rows/actions'

/**
 * Supported Redux actions.
 */
export type Actions = RowsActions
