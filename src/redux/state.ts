import { State as RowsState } from './rows/state'


export type RootState = {
  rows: RowsState,
}
